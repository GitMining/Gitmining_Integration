# 1. Paper Dataset Record

<!-- TOC -->

- [1. Paper Dataset Record](#1-paper-dataset-record)
    - [1.1. 各论文数据集描述表](#11-各论文数据集描述表)
    - [1.2. 各篇论文bug报告数据量列表](#12-各篇论文bug报告数据量列表)
        - [1.2.1. 2018](#121-2018)
            - [1.2.1.1. Improving IR-Based Bug Localization with Context-Aware Query Reformulation_2018_FSE](#1211-improving-ir-based-bug-localization-with-context-aware-query-reformulation_2018_fse)
            - [1.2.1.2. Network-Clustered Multi-Modal Bug Localization_2018_TSE](#1212-network-clustered-multi-modal-bug-localization_2018_tse)
            - [1.2.1.3. Locating bugs without looking back_2018_MSR](#1213-locating-bugs-without-looking-back_2018_msr)
        - [1.2.2. 2017](#122-2017)
            - [1.2.2.1. Using Observed Behavior to Reformulate Queries during Text Retrieval-based Bug Localization_2017_ICSME](#1221-using-observed-behavior-to-reformulate-queries-during-text-retrieval-based-bug-localization_2017_icsme)
            - [1.2.2.2. Improved bug localization based on code change histories and bug reports](#1222-improved-bug-localization-based-on-code-change-histories-and-bug-reports)
            - [1.2.2.3. An Empirical Comparison of Model Validation Techniques for Defect Prediction Models_2017_TSE](#1223-an-empirical-comparison-of-model-validation-techniques-for-defect-prediction-models_2017_tse)
        - [1.2.3. 2016](#123-2016)
            - [1.2.3.1. Locating Bugs without Looking Back_2016_MSR](#1231-locating-bugs-without-looking-back_2016_msr)

<!-- /TOC -->

## 1.1. 各论文数据集描述表

黑体加粗表示内容还没有细看，详细数据集描述需要再看。

| 论文 | 年份 | 会议/期刊 | 数据集描述 | bug报告数据量列表 |
| -- | -- | -- | -- | -- |
| 2018年份论文
| Improving IR-Based Bug Localization with Context-Aware Query Reformulation | 2018 | FSE | 从6个开源项目中收集了5139份bug报告，所有被标记为**RESOLVED**的bug报告都来自于**BugZilla**和**JIRA**，这些报告都是在特定的时间段内被提交。**利用GitHub的仓库版本控制系统来确定bug修复commits**，这里参考了[**7,34,66**]，本文也是这么做的，丢弃没有源码修改或源码不存在于snapshot中的bug报告。Bug报告中st代表stack trace， pe代表program elements，nl代表nature language。| [1.2.1.1. Improving IR-Based Bug Localization with Context-Aware Query Reformulation_2018_FSE](#1211-improving-ir-based-bug-localization-with-context-aware-query-reformulation_2018_fse) |
| Network-Clustered Multi-Modal Bug Localization | 2018 | TSE | 本文比较特殊，数据集根据bug数量来统计，没有根据bug报告的数量来统计。收集了来自七个项目的355个bug，七个项目分别是Ant [1], AspectJ [5], Lang [2], Lucene [4], Math [3], Rhino [10], and Time.其中Ant，Lucene和AspectJ超过300KLOC，Math，Rhino和Time有100KLOC，Lang有50KLOC的代码量。Ant, Lang, Lucene, 和 Math使用**Jira**作为问题跟踪系统。Jira上记录的bug通常可以很好的链接到对应的修复commit上。AspectJ和Rhino使用**Bugzilla**，Time使用**GitHub**作为问题跟踪系统。**1**）**Ant, Lucene, 和 Rhino**的116个bug是作者们自己收集的，参考了[**23**]中所记载的方法。**对于每一个bug，收集修复前版本，修复后版本，成功的测试用例集合和至少一组失败的测试用例（需要测试用例是因为本文使用基于谱的定位技术）。** **2**）41个**AspectJ**的bugs 来自Dallmeier and Zimmermann收集**iBugs**数据集[**23**]，因为iBugs中有部分bug没有测试用例，所以本文没有选择。从**Defects4J基线**收集了198个Lang, Math和Time的bugs | [1.2.1.2. Network-Clustered Multi-Modal Bug Localization_2018_TSE](#1212-network-clustered-multi-modal-bug-localization_2018_tse) |
| Locating bugs without looking back | 2018 | Proceeding MSR '16 Proceedings of the 13th International Conference on Mining Software Repositories Pages 286-290  | 与六个项目进行了对比，使用了八个数据集，其中六个是常见数据集，两个为金融应用，两个应用中，其中一个是开源的，另一个是专有软件。文章出于自身的目的，分析、统计了其他几种技术在增强准确度时所使用到的额外信息。收集的数据集包括了源文件、BR以及BR影响的文件。 | [1.2.1.3. Locating bugs without looking back_2018_MSR](#1213-locating-bugs-without-looking-back_2018_msr) |
| 2017年份论文
| Using Observed Behavior to Reformulate Queries during Text Retrieval-based Bug Localization | 2017 | ICSME | 文章使用了多种规模、涉及多个领域的21个Java开源项目的78个版本。数据集来自于Just et al.’s Defects4J testing data set (a.k.a. **D4J**), Mills et al.’s data set on query quality assessment (a.k.a. **QQ**), Moreno et al.’s bug localization data set (a.k.a. **LB**), Wong et al.’s bug localization data set (a.k.a. **BRT**) **1**）从DJ4中收集了**Lan**g, **Math**, 和**Joda-Time**124个bug的bug报告（通过人工方法，在版本控制系统中收集对应bug的bug报告ID，再在问题追踪系统中定位到），**2**）LB数据集包含了14个开源软件的一共17个版本的974个bugs它们相关的TRBL数据，比如：**bug报告**，**源码**，**修复bug的相关代码文件**，最终使用了815份br。**3**）QQ的数据集包含了12个开源软件的一共15个版本，一共278个bugs和与之相关的TRBL数据，最终使用了11个软件的13个版本，一共241个bug。**4**）BRT数据集包含了3个开源系统的3459个bug, 最终剔除了AspectJ之后，使用了**Eclipse**和**SWT**的3173个bug.| [1.2.2.1. Using Observed Behavior to Reformulate Queries during Text Retrieval-based Bug Localization_2017_ICSME](#1221-using-observed-behavior-to-reformulate-queries-during-text-retrieval-based-bug-localization_2017_icsme) |
| Improved bug localization based on code change histories and bug reports | 2017 | infsof | 使用了三个开源软件AspectJ, SWT, ZXing以及修复bug相关的文件。利用了Zhou et al. [19] 在BugLocator和Wong et al. [23] 在BRTracer中使用到的数据集。对于收集到的BR，在仓库中检查对应的修复commit，利用"git diff"命令获取了最后修复bug的相关文件和commit日志。 | [1.2.2.2. Improved bug localization based on code change histories and bug reports](#1222-improved-bug-localization-based-on-code-change-histories-and-bug-reports) |
| ***An Empirical Comparison of Model Validation Techniques for Defect Prediction Models*** | 2017 | TSE | 数据集选择需要满足三个条件，条件一：必须要有不同的语料库和领域，条件二：充足的EPV，所选系统的EPV应该开始时处于低风险的EPV。条件三：完整的缺陷数据。数据集包含了各种规模、领域、缺陷率的OOS和商业软件。(暂时只记录了数据量，没有来得及细看文章内容，计算EPV时应该会有其他信息参与)| [1.2.2.3. An Empirical Comparison of Model Validation Techniques for Defect Prediction Models_2017_TSE](#1223-an-empirical-comparison-of-model-validation-techniques-for-defect-prediction-models_2017_tse) |
| 2016年份论文
| *Practitioners’ Expectations on Automated Fault Localization* | 2016 | ISSTA |　没有涉及到具体数据集，但是扫一眼感觉比较有意思，可以之后一看 |
| Locating Bugs without Looking Back | 2016 | MSR | 这是一篇比较有意思的文章，通常BL的技术都会使用额外的信息来提高定位的准确度，但是本文使用了一种比较独特的方法，没有使用额外信息，直接比较代码文件和bug报告。与五种现有技术进行对比，并且使用了五种技术的六个数据集。但是只涉及了BR和源码，没有涉及其他信息。 | [1.2.3.1. Locating Bugs without Looking Back_2016_MSR](#1231-locating-bugs-without-looking-back_2016_msr)

## 1.2. 各篇论文bug报告数据量列表

### 1.2.1. 2018

#### 1.2.1.1. Improving IR-Based Bug Localization with Context-Aware Query Reformulation_2018_FSE

| System | Period | BRst | BRpe | BRnl | BRall |
| -- | -- | -- | -- | -- | -- |
| ecf |2001-10 2017-1 | 71 | 319 | 163 | 553 |
| eclipse.jdt.core | Oct, 2001–Sep, 2016 | 159 | 698 | 132 | 989 |
| eclipse.jdt.debug | Oct, 2001–Jan, 2017 | 126 | 202 | 229| 557 |
| eclipse.jdt.ui | Oct, 2001–Jun, 2016 | 130 | 578 | 407 | 1,115 |
| eclipse.pde.ui | Oct, 2001–Jun, 2016 | 123 | 239 | 510 | 872 |
| tomcat70 | Sep, 2001–Aug, 2016 | 217 | 731 | 105 | 1,053 |

#### 1.2.1.2. Network-Clustered Multi-Modal Bug Localization_2018_TSE

| System | Bugs | Period | Average Method | Issue Trace System | From |
| -- | -- | -- | -- | -- | -- |
| Apache-Ant | 53 | 12/2001 – 09/2013 | 9,624.66 | Jira | collect by themself |
| AspectJ | 41 | 03/2005 – 02/2007 | 14,218.39 | Bugzilla | iBugs |
| Apache-Commons-Lang | 65 | 10/2002 – 04/2016 | 2,151.1 | Jira | Defects4J基线 |
| Lucene | 37 | 06/2006 – 01/2011 | 10,220.14 | Jira | collect by themself |
| Apache-Commons-Math | 106 | 12/2004 – 03/2016 | 4,792.3 | Jira | Defects4J基线 |
| Rhino | 26 | 12/2007 – 12/2011 | 4,839.58 | Bugzilla | collect by themself |
| Joda-Time | 27 | 05/2004 – 03/2017 | 4,083.5 | GitHub | Defects4J基线 |

#### 1.2.1.3. Locating bugs without looking back_2018_MSR

| Project | Source files | Bug reports | Period | Used also by |
| -- | -- | -- | -- | -- |
| AspectJ | 6485 | 286 | 2002/07–2006/10 | Zhou et al. (2012), Saha et al. (2013),Wong et al. (2014), Wang and Lo (2014) and Youm et al. (2015) |
| Eclipse | 12863 | 3075 | 2004/10–2011/03 | Zhou et al. (2012), Saha et al. (2013), Wong et al. (2014) and Wang and Lo (2014) |
| SWT | 484 | 98 | 2004/10–2010/04 | Zhou et al. (2012), Saha et al. (2013), Wong et al. (2014), Wang and Lo (2014), Youm et al. (2015) and Rahman et al. (2015) |
| ZXing | 391 | 20 | 2010/03–2010/09 | Zhou et al. (2012), Youm et al. (2015) and Rahman et al. (2015) |
| Tomcat | 2038 | 1056 | 2002/07–2014/01 | Ye et al. (2014) ArgoUML 1685 91 2002/01–2006/07 Moreno et al. (2014) |
| Pillar1 | 4355 | 27 | 2012/03–2013/01 | – |
| *Pillar2 | 337 | 12 | 2010/05–2011/01 | Dilshener and Wermelinger (2011) |

文章出于自身的目的，分析、统计了其他几种技术在增强准确度时所使用到的额外信息

| Approach | Underlying IR logic | Version history | Similar report | Structure | File name | Stack trace |
| -- | -- | -- | -- | -- | -- | -- |
| BugLocator | rVSM | 0 | 1 | 0 | 0 | 0 |
| BRTracer | rVSM + segmentation | 0 | 1 | 1 | 0 | 1 |
| BLUiR | Indri | 0 | 1 | 1 | 0 | 0 |
| AmaLgam | Mixed | 1 | 1 | 1 | 0 | 0 |
| BLIA | rVSM + segmentation | 1 | 1 | 1 | 0 | 1 |
| Rahman | rVSM | 1 | 1 | 0 | 1 | 0 |
| LtR | VSM | 1 | 1 | 1 | 0 | 0 |
| LOBSTER | VSM | 0 | 0 | 1 | 0 | 1 |
| ConCodeSe | lexicalScore + VSM | 0 | 0 | 1 | 1 | 1 |

### 1.2.2. 2017

#### 1.2.2.1. Using Observed Behavior to Reformulate Queries during Text Retrieval-based Bug Localization_2017_ICSME

| Dataset  | Code granularity | # of systemsa | # of coded bug reports | # of bug reports containing OB | # of low-quality reduced queries | Software |
| -- | -- | -- | -- | -- | -- | -- |
| D4J | Method | 3 (52) | 36 | 35 (97.2%) | 35 | Lang + Math + Joda-Time
| QQ | Method | 11 (13) | 103 | 100 (97.1%) | 100 | 13 version of 11 software
| LB | Class | 13 (16) | 199 | 197 (99.0%) | 196 | 17 version of 14 OOS
| BRT | File | 2 (2) | 130 | 125 (96.2%) | 125 | Eclipse + SWT
| Overall |  | 21 (78) | 462 | 452 (97.8%) | 451

#### 1.2.2.2. Improved bug localization based on code change histories and bug reports

Project | Description | Period of registered bug (Used source version) | #Bugs | #Source Files
-- | -- | -- | -- | --
AspectJ | Aspect-oriented extension to Java | 12/20 02 ∼ 07/20 07 (org.aspectj-1_5_3_final) | 284 | 5188
SWT | Widget toolkit for Java | 04/20 02 ∼ 12/20 05 (swt-3.659) | 98 | 738
ZXing | Barcode image processing library | 03/2010 ∼ 09/2010(Zxing-1.6) | 20 | 391

#### 1.2.2.3. An Empirical Comparison of Model Validation Techniques for Defect Prediction Models_2017_TSE

Project | System | Defective Ratio | #Files | #Metrics | EPV
--  | -- | -- | -- | -- | --
| NASA | JM1-1 | 21% | 7,782 | 21 | 80
|  |PC5-1 | 28% | 1,711 | 38 | 12
| Proprietary | Prop-1-2 | 15% | 18,471 | 20 | 137
|  |Prop-2-2 | 11% | 23,014 | 20 | 122
|  |Prop-3-2 | 11% | 10,274 | 20 | 59
|  |Prop-4-2 | 10% | 8,718 | 20 | 42
|  |Prop-5-2 | 15% | 8,516 | 20 | 65
| Apache | Camel 1.2-2 | 36% | 608 | 20 | 11
|  |Xalan | 2.5-2 | 48% | 803 | 20 | 19
|  |Xalan | 2.6-2 | 46% | 885 | 20 | 21
| Eclipse | Platform 2.0-3 | 14% | 6,729 | 32 | 30
|  |Platform 2.1-3 | 11% | 7,888 | 32 | 27
|  |Platform 3.0-3 | 15% | 10,593 | 32 | 49
|  |Debug 3.4-4 | 25% | 1,065 | 17 | 15
|  |SWT 3.4-4 | 44% | 1,485 | 17 | 38
|  |JDT-5 | 21% | 997 | 15 | 14
|  |Mylyn-5 | 13% | 1,862 | 15 | 16
|  |PDE-5 | 14% | 1,497 | 15 | 14

### 1.2.3. 2016

#### 1.2.3.1. Locating Bugs without Looking Back_2016_MSR

Project | Source files | BRs | BR Period | Note
-- | -- | -- | -- | --
AspectJ | 6485 | 286 | 2002/07 – 2006/10 | the aspect-oriented programming library
Eclipse | 12863 | 3075 | 2004/10 – 2011/03 | the IDE tool
SWT | 484 | 98 | 2004/10 – 2010/04 | a GUI subproject of Eclipse
ZXing | 391 | 20 | 2010/03 – 2010/09 | an Android project
Tomcat | 2038 | 1056 | 2002/07 – 2014/01 |
ArgoUML | 1685 | 91 | 2002/01 – 2006/07 |